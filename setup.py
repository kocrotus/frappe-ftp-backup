from setuptools import setup, find_packages

with open("requirements.txt") as f:
	install_requires = f.read().strip().split("\n")

# get version from __version__ variable in mempeng_ftp_backup/__init__.py
from mempeng_ftp_backup import __version__ as version

setup(
	name="mempeng_ftp_backup",
	version=version,
	description="Scheduler untuk backup data erpnext ke FTP server",
	author="EduKreasi",
	author_email="admin@edukreasi.co.id",
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=install_requires
)
