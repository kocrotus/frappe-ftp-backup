// Copyright (c) 2021, EduKreasi and contributors
// For license information, please see license.txt

frappe.ui.form.on('Pengaturan Backup FTP', {
		setup: function (frm) {
            //do something on setup mode
            frappe.realtime.on('ftp_realtime_backup', data =>  {
                frappe.show_progress(data.title, data.current_progress,data.overall,data.desc,true);

                //setTimeout(() => {
                //    frappe.show_progress(data.title, data.current_progress,data.overall,data.desc,true);
                //}
                //, 1000);

                // hide progress when complete
                if (data.done) {
                    setTimeout(() => {
                        frappe.hide_progress();
                    }
                    , 1000);
                }
            });
		},

		refresh: function (frm) {
            //do something on page refresh mode

		}
	}
);
