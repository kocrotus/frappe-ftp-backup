# Copyright (c) 2021, EduKreasi and contributors
# For license information, please see license.txt

import sys
import os
import tempfile
#import time
from datetime import datetime,timedelta

from frappe.model.document import Document
import frappe
from frappe.utils import split_emails,cint
from frappe.utils import get_files_path, get_backups_path, get_url, encode
from frappe.utils.backups import new_backup
from ftplib import FTP, FTP_TLS, error_perm

#todo: deploy : set to False on deployment
DEBUG = False

#this is the size imit for chunk mode upload
SIZE_LIMIT = 1024*1024 #1 MB

class PengaturanBackupFTP(Document):
    @frappe.whitelist()
    def exec_ftp_test_conn(self):
        #tester
        #frappe.msgprint('say hello from test conn')

        ftp_active, ftp_type, \
        weekly_exec_time, dailly_exec_time, monthly_exec_date, \
        ftp_server, ftp_tls, ftp_username, \
        ftp_password, ftp_path, ftp_max_files, \
        ftp_email_after_exec, ftp_email, job_is_exec  = get_db_ftp_settings()

        if exec_ftp_test_process(
            ftp_tls,ftp_server,ftp_username,ftp_password
        ):
            frappe.msgprint(
                msg='Tes server FTP - '+ftp_server+' - sukses',
                title='Informasi',
                indicator='green'
            )
        else:
            frappe.msgprint(
                msg='Tes server FTP - ' + ftp_server + ' - GAGAL !',
                title='Peringatan',
                indicator='red'
            )

    @frappe.whitelist()
    def exec_ftp_backup_now(self):
        # tester
        #frappe.msgprint('say hello from backup now')

        ftp_active, ftp_type, \
        weekly_exec_time, dailly_exec_time, monthly_exec_date, \
        ftp_server, ftp_tls, ftp_username, \
        ftp_password, ftp_path, ftp_max_files, \
        ftp_email_after_exec, ftp_email, job_is_exec  = get_db_ftp_settings()

        if exec_backup_process(
            ftp_email_after_exec,ftp_email,
            ftp_tls,ftp_server,ftp_username,ftp_password,ftp_path,cint(ftp_max_files),True
        ):
            frappe.msgprint(
                msg='Backup data ke server FTP berhasil',
                title='Informasi',
                indicator='green'
            )
        else:
            frappe.msgprint(
                msg='Backup data ke server FTP GAGAL !',
                title='Peringatan',
                indicator='red'
            )


    def validate(self):
        # get all settings
        ftp_active, ftp_type, \
        weekly_exec_time, dailly_exec_time, monthly_exec_date, \
        ftp_server, ftp_tls, ftp_username, \
        ftp_password, ftp_path, ftp_max_files, \
        ftp_email_after_exec, ftp_email , job_is_exec = self.get_ftp_settings()

        #validate the number of backups
        if not cint(ftp_max_files):
            frappe.throw(
                msg='Jumlah max file backup harus berupa angka !',
                title='Peringatan'
            )


        """
        frappe.msgprint(
            msg='Test ftp domain :' + ftp_server,
            title='Peringatan',
            indicator='red'
        )

        frappe.throw('Test validate')
        """


    def get_ftp_settings(self):
        # get the ftp settings data from the doc type : can be usefull for validation
        settings = frappe.get_doc("Pengaturan Backup FTP")

        return \
            settings.schedule_ftp_active, \
            settings.schedule_type, \
            settings.weekly_exec, \
            settings.dailly_exec, \
            settings.monthly_exec_date, \
            settings.ftp_server.strip(), \
            settings.ftp_tls, \
            settings.ftp_username.strip(), \
            settings.ftp_password.strip(), \
            settings.ftp_path.strip(), \
            settings.ftp_max_files, \
            settings.ftp_email_after_exec, \
            settings.ftp_email,\
            settings.status_is_exec
            #settings.ftp_passive

def get_db_ftp_settings():
    # get the ftp settings data from the DB
    if not frappe.db:
        frappe.connect()

    return \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'schedule_ftp_active'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'schedule_type'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'weekly_exec'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'dailly_exec'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'monthly_exec_date'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_server').strip(), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_tls'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_username').strip(), \
        frappe.get_doc("Pengaturan Backup FTP").get_password('ftp_password'),\
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_path').strip(), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_max_files'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_email_after_exec'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_email'), \
        frappe.db.get_value('Pengaturan Backup FTP', None, 'status_is_exec')
        #frappe.db.get_value('Pengaturan Backup FTP', None, 'ftp_passive')


#non class member

def perform_backup_check_scheduler():
    #cron b ased scheduler system
    ftp_active, ftp_type, \
    weekly_exec_time, dailly_exec_time, monthly_exec_date, \
    ftp_server, ftp_tls, ftp_username, \
    ftp_password, ftp_path, ftp_max_files, \
    ftp_email_after_exec, ftp_email, job_is_exec  = get_db_ftp_settings()

    log('Perform scheduler event. time of backup : '+dailly_exec_time+' status aktif :'+str(ftp_active)+' , schedule type : '+ftp_type+
        ' , job is exec : '+job_is_exec,
        'FTP Backup - Scheduler Action')

    proceed = False
    perform_backup = False

    #perform check of sceduler task is in action or not
    if cint(ftp_active)==1:
        if cint(job_is_exec)==0:
            #step : set the value to 1 to perform the check
            frappe.db.set_value('Pengaturan Backup FTP', None, 'status_is_exec',1)

            current_date_time=datetime.now()
            week_start=None
            week_end=None
            target_db_backup_fn = 'erp_backup_db_' + current_date_time.strftime('_%Y_%m_%d')+'.gz'

            #step : do the date and time validation
            if ftp_type=='Harian':
                list_time=dailly_exec_time.split(':')
                if len(list_time)==3:
                    #create a date of backup
                    date_backup_str=str(current_date_time.day)+':'+str(current_date_time.month)+':'+str(current_date_time.year)+\
                        ':'+list_time[0]+':'+list_time[1]+':'+list_time[2]

                    date_backup=datetime.strptime(date_backup_str,'%d:%m:%Y:%H:%M:%S')

                    log('current date : ' + current_date_time.strftime('%d:%m:%Y:%H:%M:%S') + ' , backup date :' +
                        date_backup.strftime('%d:%m:%Y:%H:%M:%S'), 'FTP Backup - Scheduler Action')

                    if (current_date_time>=date_backup) :
                        proceed=True
            elif ftp_type=='Mingguan':
                week_int=-1
                if weekly_exec_time=='Minggu':
                    week_int=0
                elif weekly_exec_time=='Senin':
                    week_int=1
                elif weekly_exec_time=='Selasa':
                    week_int=2
                elif weekly_exec_time=='Rabu':
                    week_int=3
                elif weekly_exec_time=='Kamis':
                    week_int=4
                elif weekly_exec_time=='Jumat':
                    week_int=5
                elif weekly_exec_time=='Sabtu':
                    week_int=6

                if (current_date_time.weekday()==week_int):
                    list_time = dailly_exec_time.split(':')
                    if len(list_time) == 3:
                        #get the start and end of week
                        week_start=current_date_time-timedelta(days=current_date_time.weekday())
                        week_end=week_start+timedelta(days=6)

                        #check the time
                        date_backup_str = str(current_date_time.day) + ':' + str(current_date_time.month) + ':' + str(
                            current_date_time.year) + \
                                          ':'+list_time[0]+':'+list_time[1]+':'+list_time[2]

                        date_backup = datetime.strptime(date_backup_str, '%d:%m:%Y:%H:%M:%S')

                        if (current_date_time >= date_backup) :
                            proceed = True
            elif ftp_type == 'Bulanan':
                if (current_date_time.day)==cint(monthly_exec_date):
                    list_time = dailly_exec_time.split(':')
                    if len(list_time) == 3:
                        # check the time
                        date_backup_str = str(current_date_time.day) + ':' + str(current_date_time.month) + ':' + str(
                            current_date_time.year) + \
                                          ':'+list_time[0]+':'+list_time[1]+':'+list_time[2]

                        date_backup = datetime.strptime(date_backup_str, '%d:%m:%Y:%H:%M:%S')

                        if (current_date_time >= date_backup):
                            proceed = True

            if proceed:
                # step : check the ftp server for files availability
                ftp_client = None
                try:
                    # step : perform connection to ftp server and prep path
                    if ftp_tls == 1:
                        ftp_client = FTP_TLS(host=ftp_server, user=ftp_username, passwd=ftp_password)
                    else:
                        ftp_client = FTP(host=ftp_server, user=ftp_username, passwd=ftp_password)

                    #list the filename in the db path
                    path_target = ftp_path.rstrip('/') + '/database/'
                    list_names = ftp_client.nlst(path_target)

                    log('continue backup process with ftp list names' +str(list_names)
                        , 'FTP Backup - Scheduler Action')

                    found_file=False
                    for item in list_names:
                        if item==target_db_backup_fn:
                            found_file=True

                    if not found_file:
                        proceed=True
                        perform_backup=True
                except:
                    proceed = False
                    perform_backup=False

                    error_msg = 'check phase : ftp connection test failed for server : ' + ftp_server + \
                                ' , user : ' + ftp_username + \
                                ' :  %s %s' % (sys.exc_info()[0], sys.exc_info()[1])

                    frappe.log_error(error_msg, 'FTP Backup - Scheduler')

                if ftp_client is not None:
                    ftp_client.quit()

            #step : perform the backup if necessary
            if perform_backup:
                log('Perform scheduler event. continue backup with', 'FTP Backup - Scheduler Action')

                if not exec_backup_process(
                        ftp_email_after_exec, ftp_email,
                        ftp_tls, ftp_server, ftp_username, ftp_password, ftp_path, cint(ftp_max_files), False
                ):
                    frappe.log_error('Proses scheduler - backup FTP mengalami kegagalan', 'FTP Backup - Scheduler')

                #we are done, now set the value
                frappe.db.set_value('Pengaturan Backup FTP', None, 'status_is_exec', 0)

        if not perform_backup:
            # step : set the status to zero again if not doing anything
            log('Perform scheduler event. NOT Doing backup ', 'FTP Backup - Scheduler Action')
            frappe.db.set_value('Pengaturan Backup FTP', None, 'status_is_exec', 0)


def exec_ftp_test_process(
                        ftp_is_tls, ftp_server, ftp_user, ftp_password
                        ):
    result=False

    ftp_client = None
    try:
        # step : perform connection to ftp server and prep path
        if ftp_is_tls == 1:
            ftp_client = FTP_TLS(host=ftp_server, user=ftp_user, passwd=ftp_password)
        else:
            ftp_client = FTP(host=ftp_server, user=ftp_user, passwd=ftp_password)

        result = True
    except:
        result = False
        error_msg = 'test ftp connection failed for server : '+ftp_server+\
                    ' , user : '+ftp_user+' , ftp password : '+ftp_password+\
                    ' :  %s %s' % (sys.exc_info()[0], sys.exc_info()[1])
        frappe.log_error(error_msg,'FTP Backup - test connection failed')

    if ftp_client is not None:
        ftp_client.quit()

    return result


def exec_backup_process(
                        is_send_mail,
                        email_accs,
                        ftp_is_tls, ftp_server, ftp_user, ftp_password, ftp_path, ftp_no_of_backups,from_backup_now
                        #ftp_is_passive
                        ):
    result=False

    # perform backup process
    ftp_client = None
    try:
        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                    {
                        'title':'Eksekusi Backup ke FTP Server',
                        'desc':'Memproses backup lokal ...',
                        'current_progress':1,
                        'overall': 7,'done':False
                    }
                )


        # step : perform the frappe backup process
        backup = new_backup(
            ignore_files=False,
            compress=True
        )

        log('FTP Backup - backup process DEBUG','Test A')

        backup_fn_db = os.path.join(get_backups_path(), os.path.basename(backup.backup_path_db))
        backup_fn_files = os.path.join(get_backups_path(), os.path.basename(backup.backup_path_files))
        backup_fn_priv = os.path.join(get_backups_path(), os.path.basename(backup.backup_path_private_files))

        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                {
                'title':'Eksekusi Backup ke FTP Server', 'desc':'Melakukan koneksi ke server ...',
                    'current_progress':2,'overall': 7, 'done':False
                }
            )

        # step : perform connection to ftp server and prep path
        if ftp_is_tls==1:
            ftp_client = FTP_TLS(host=ftp_server, user=ftp_user, passwd=ftp_password)
        else:
            ftp_client = FTP(host=ftp_server, user=ftp_user, passwd=ftp_password)

        log('FTP Backup - backup process DEBUG', 'Test B')

        '''
        # step : set passive mode or not
        if ftp_is_passive==1:
            ftp_client.set_pasv(True)
        else:
            ftp_client.set_pasv(False)
        '''


        path_db=''
        path_files=''
        path_private=''

        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                {
                    'title':'Eksekusi Backup ke FTP Server','desc':'Memproses database ...',
                    'current_progress':3, 'overall':7,'done':False
                }
            )

        current=datetime.now()

        # step 1 : perform upload database
        file_size=os.stat(backup_fn_db).st_size
        log('FTP Backup - backup process DEBUG', 'Test Upload DB file size : '+backup_fn_db+' => '+str(file_size))
        with open(encode(backup_fn_db), 'rb') as f:
            # set the filename of the remote backup
            remote_fn = 'erp_backup_db_' + current.strftime('_%Y_%m_%d')+'.gz'
            path_target=ftp_path.rstrip('/')+'/database/'
            path_db=path_target

            try:
                create_folder_if_not_exists(ftp_client, path_target)
                pwd = ftp_client.pwd()
                ftp_client.cwd(path_target)

                if file_size<=SIZE_LIMIT:
                    ftp_client.storbinary('STOR %s' % remote_fn, f)
                else:
                    #chunk upload mode
                    log('FTP Backup - backup process DEBUG', 'Test Upload DB Chunk')
                    rest=None
                    chunk = f.read(SIZE_LIMIT)

                    #create the temp file
                    tempf = tempfile.NamedTemporaryFile(delete=False)
                    temp_namer = tempf.name
                    tempf.write(chunk)
                    tempf.close()

                    while chunk:
                        log('FTP Backup - backup process DEBUG',
                            'Test Upload DB Chunk start '+str(rest)+' with length : '+str(os.stat(temp_namer).st_size) )

                        with open(encode(temp_namer), 'rb') as tempf:
                            ftp_client.storbinary('STOR %s' % remote_fn, tempf ,
                                                  #callback=debug_handle_ftp,
                                                  rest=rest)

                        # remove the temp file
                        os.remove(temp_namer)

                        if rest is None:
                            rest=len(chunk)
                        else:
                            rest+=len(chunk)

                        #read again the next one
                        log('FTP Backup - backup process DEBUG','Test Upload DB Chunk after ' + str(rest) + ' with length : ' + str(len(chunk)))
                        chunk = f.read(SIZE_LIMIT)

                        #create the temp file
                        tempf = tempfile.NamedTemporaryFile(delete=False)
                        temp_namer = tempf.name
                        tempf.write(chunk)
                        tempf.close()

                    #remove final file if exists
                    if os.path.exists(temp_namer):
                        os.remove(temp_namer)


                ftp_client.cwd(pwd)
            except Exception:
                error = 'Upload DB process error\n'
                error += frappe.get_traceback()
                frappe.log_error(error,'Upload FTP - db file failed')
                raise Exception(error)

        log('FTP Backup - backup process DEBUG', 'Test C')

        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                {
                    'title':'Eksekusi Backup ke FTP Server', 'desc':'Memproses file umum ...',
                    'current_progress':4, 'overall':7, 'done':False
                }
            )

        # step 2 : perform upload standard files
        file_size = os.stat(backup_fn_files).st_size
        with open(encode(backup_fn_files), 'rb') as f:
            # set the filename of the remote backup
            remote_fn = 'erp_backup_files_' + current.strftime('_%Y_%m_%d')+'.tgz'
            path_target = ftp_path.rstrip('/')+'/files/'
            path_files = path_target

            try:
                create_folder_if_not_exists(ftp_client, path_target)
                pwd = ftp_client.pwd()
                ftp_client.cwd(path_target)

                if file_size <= SIZE_LIMIT:
                    ftp_client.storbinary('STOR %s' % remote_fn, f)
                else:
                    #chunk upload mode
                    log('FTP Backup - backup process DEBUG', 'Test Upload standard Chunk')
                    rest=None
                    chunk = f.read(SIZE_LIMIT)

                    #create the temp file
                    tempf = tempfile.NamedTemporaryFile(delete=False)
                    temp_namer = tempf.name
                    tempf.write(chunk)
                    tempf.close()

                    while chunk:
                        log('FTP Backup - backup process DEBUG',
                            'Test Upload standard Chunk start '+str(rest)+' with length : '+str(os.stat(temp_namer).st_size) )

                        with open(encode(temp_namer), 'rb') as tempf:
                            ftp_client.storbinary('STOR %s' % remote_fn, tempf ,
                                                  #callback=debug_handle_ftp,
                                                  rest=rest)

                        # remove the temp file
                        os.remove(temp_namer)

                        if rest is None:
                            rest=len(chunk)
                        else:
                            rest+=len(chunk)

                        #read again the next one
                        log('FTP Backup - backup process DEBUG','Test Upload standard Chunk after ' + str(rest) + ' with length : ' + str(len(chunk)))
                        chunk = f.read(SIZE_LIMIT)

                        #create the temp file
                        tempf = tempfile.NamedTemporaryFile(delete=False)
                        temp_namer = tempf.name
                        tempf.write(chunk)
                        tempf.close()

                    #remove final file if exists
                    if os.path.exists(temp_namer):
                        os.remove(temp_namer)

                ftp_client.cwd(pwd)
            except Exception:
                error = 'Upload standard files process error\n'
                error += frappe.get_traceback()
                frappe.log_error(error, 'Upload FTP - regular file failed')
                raise Exception(error)

        log('FTP Backup - backup process DEBUG', 'Test D')

        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                {
                    'title':'Eksekusi Backup ke FTP Server','desc':'Memproses file personal ...',
                    'current_progress':5,'overall': 7, 'done':False
                }
            )



        # step 3 : perform upload privated files
        file_size = os.stat(backup_fn_priv).st_size
        with open(encode(backup_fn_priv), 'rb') as f:
            # set the filename of the remote backup
            remote_fn = 'erp_backup_priv_' + current.strftime('_%Y_%m_%d')+'.tgz'
            path_target = ftp_path.rstrip('/')+'/private/'
            path_private = path_target

            try:
                create_folder_if_not_exists(ftp_client, path_target)
                pwd = ftp_client.pwd()
                ftp_client.cwd(path_target)

                if file_size <= SIZE_LIMIT:
                    ftp_client.storbinary('STOR %s' % remote_fn, f)
                else:
                    #chunk upload mode
                    log('FTP Backup - backup process DEBUG', 'Test Upload private Chunk')
                    rest=None
                    chunk = f.read(SIZE_LIMIT)

                    #create the temp file
                    tempf = tempfile.NamedTemporaryFile(delete=False)
                    temp_namer = tempf.name
                    tempf.write(chunk)
                    tempf.close()

                    while chunk:
                        log('FTP Backup - backup process DEBUG',
                            'Test Upload private Chunk start '+str(rest)+' with length : '+str(os.stat(temp_namer).st_size) )

                        with open(encode(temp_namer), 'rb') as tempf:
                            ftp_client.storbinary('STOR %s' % remote_fn, tempf ,
                                                  #callback=debug_handle_ftp,
                                                  rest=rest)

                        # remove the temp file
                        os.remove(temp_namer)

                        if rest is None:
                            rest=len(chunk)
                        else:
                            rest+=len(chunk)

                        #read again the next one
                        log('FTP Backup - backup process DEBUG','Test Upload private Chunk after ' + str(rest) + ' with length : ' + str(len(chunk)))
                        chunk = f.read(SIZE_LIMIT)

                        #create the temp file
                        tempf = tempfile.NamedTemporaryFile(delete=False)
                        temp_namer = tempf.name
                        tempf.write(chunk)
                        tempf.close()

                    #remove final file if exists
                    if os.path.exists(temp_namer):
                        os.remove(temp_namer)

                ftp_client.cwd(pwd)
            except Exception:
                error = 'Upload private files process error\n'
                error += frappe.get_traceback()
                frappe.log_error(error, 'Upload FTP - private file failed')
                raise Exception(error)

        log('FTP Backup - backup process DEBUG', 'Test E')

        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                {
                    'title':'Eksekusi Backup ke FTP Server','desc':'Menyesuaikan sistem server ...',
                    'current_progress':6,'overall': 7, 'done':False
                }
            )

        # step : remove any excess files that limit the number of backups
        #just send warning on error about this process
        try:
            adjust_storage_amount_backup(ftp_client, path_db, ftp_no_of_backups)
            adjust_storage_amount_backup(ftp_client, path_files, ftp_no_of_backups)
            adjust_storage_amount_backup(ftp_client, path_private, ftp_no_of_backups)
        except:
            error_msg = 'adjusting storage amount of backups failed :  %s %s' % (sys.exc_info()[0], sys.exc_info()[1])
            frappe.log_error(error_msg, 'WARNING - FTP Backup - backup process adjustment')

        log('FTP Backup - backup process DEBUG', 'Test F')

        if from_backup_now:
            frappe.publish_realtime('ftp_realtime_backup',
                {
                    'title':'Eksekusi Backup ke FTP Server', 'desc':'Finalisasi ...',
                    'current_progress':7, 'overall':7, 'done':True
                }
            )


        # step : done action
        if is_send_mail==1:
            # just send warning on error about this process
            try:
                exec_send_mail(email_accs, True, current.strftime('_%Y_%m_%d'))
            except:
                error_msg = 'sending email after backup failed :  %s %s' % (
                sys.exc_info()[0], sys.exc_info()[1])
                frappe.log_error(error_msg, 'WARNING - FTP Backup - backup process email notification')

        log('FTP Backup - backup process DEBUG', 'Test G')

        result=True
    except:
        result=False
        error_msg = 'backup failed :  %s %s' % (sys.exc_info()[0], sys.exc_info()[1])
        frappe.log_error(error_msg, 'FTP Backup - backup process failed')
        if is_send_mail==1:
            exec_send_mail(email_accs,False,error_msg)

    if ftp_client is not None:
        ftp_client.quit()



    return result

def create_folder_if_not_exists(ftp_client, path):
    # create a new folder if not exist
    pwd = ftp_client.pwd()

    try:
        ftp_client.cwd(path)
    except error_perm:
        #log('Upload FTP - db file failed','path will be created : '+path)
        ftp_client.mkd(path)
        ftp_client.cwd(path)

    ftp_client.cwd(pwd)

def check_timestamp_files(ftp_client, filenames):
    # get original timestamp of upload
    #encure the caller has already change current dir to the wanted area
    latest_time = None

    for name in filenames:
        time = ftp_client.voidcmd("MDTM " + name)
        if (latest_time is None) or (time > latest_time):
            latest_time = time

        yield name, latest_time

def adjust_storage_amount_backup( ftp_client, folder_path, no_of_backups):
    # get the name list
    #log('FTP Backup - adjust_storage_amount_backup', 'Test path : '+folder_path)
    list_names = ftp_client.nlst(folder_path)

    #log('FTP Backup - adjust_storage_amount_backup', 'State 1')

    # loop to find the time data
    ftp_client.cwd(folder_path)

    #log('FTP Backup - adjust_storage_amount_backup', 'State 2')

    files = []
    for ft in check_timestamp_files(ftp_client, list_names):
        files.append(ft)

    #log('FTP Backup - adjust_storage_amount_backup', 'State 3')

    # return immediately if number of backups is still below the threshold
    if len(files) <= no_of_backups:
        return

    #log('FTP Backup - adjust_storage_amount_backup', 'State 4')

    # sort the filenames descending
    files.sort(key=lambda item: item[1], reverse=True)

    # remove the files from n'th max backup files
    for fn, latest_time in files[no_of_backups:]:
        ftp_client.delete(fn)

    #log('FTP Backup - adjust_storage_amount_backup', 'State done')

def exec_send_mail(email_accs, is_success, error_msg):
    if is_success:
        subject = "Backup ERP - FTP Sukses"
        message = """<h3>Proses backup data ERP ke FTP telah sukses dilakukan pada %s </h3><p>Ini adalah pesan otomatis dari
        petugas backup FTP  di modul ERP.</p>
        """ % error_msg
    else:
        subject = "!!! Backup ERP - FTP Gagal !!!"
        message = """<h3>Proses backup data ERP ke FTP gagal dilakukan</h3><p>Proses backup ke FTP server gagal dilakukan dengan notifikasi kesalahan sebagai berikut : </p>
        <pre><code>%s</code></pre>
        </p>
        <p>Silahkan kontak administrator sistem ERP untuk investigasi lebih lanjut.</p>
        """ % error_msg

    recipients = [email_accs]
    frappe.sendmail(recipients=recipients, subject=subject, message=message)


def log(title,msg):
    if DEBUG:
        frappe.log_error(msg, title)

def debug_handle_ftp(block):
    #global size_written
    #global total_size
    #global f_blocksize
    #size_written = size_written + f_blocksize if size_written + f_blocksize < total_size else total_size
    #percent_complete = size_written / total_size * 100
    #print("%s percent complete" %str(percent_complete))
    log('FTP Backup - backup process DEBUG', 'Store binary phase')