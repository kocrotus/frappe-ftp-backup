Simple Frappe module for backing up data to FTP Server

Deploy : using bench 
```
bench get-app mempeng_ftp_backup https://bitbucket.org/kocrotus/frappe-ftp-backup.git
```


Install to site : using bench
```
bench --site localhost install-app mempeng_ftp_backup
```


