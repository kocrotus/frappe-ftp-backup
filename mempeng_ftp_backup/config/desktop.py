from frappe import _

def get_data():
	return [
		{
			"module_name": "FTP Backup Scheduler",
			"color": "grey",
			"icon": "octicon octicon-file-directory",
			"type": "module",
			"label": _("FTP Backup Scheduler")
		}
	]
